FROM scratch
COPY backend-service-users /app/backend-service-users
ENTRYPOINT ["/app/backend-service-users"]
